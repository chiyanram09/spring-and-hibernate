package com.myproject.onlineshop.model;

import java.util.List;


public interface ProductDAO {

    boolean insertProductAction(Product p);

    List<Product> listProduct();
}
