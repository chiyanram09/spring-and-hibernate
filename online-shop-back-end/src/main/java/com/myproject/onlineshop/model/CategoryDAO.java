package com.myproject.onlineshop.model;

import java.util.List;


public interface CategoryDAO {

    boolean addCategory(Category c);

    List<Category> listCategory();

    Category findById(int id);

}
