package com.myproject.onlineshop.model;

import java.util.List;

public interface SupplierDAO {
    boolean addSupplier(Supplier s);

    List<Supplier> listSupplier();
}
